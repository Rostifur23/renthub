import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from './product.model';
import flatpickr from "flatpickr";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  
  product: {pid: number, name: string, type: string, daily_charge: number, available: boolean, prod_pic: string};

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {


    console.log(this.route.snapshot.params['id']);

    this.product = {
      pid: this.route.snapshot.params['id'],
      name: 'prod-name',
      type: 'Technology',
      daily_charge: 39,
      available: true,
      prod_pic: 'https://c1.staticflickr.com/6/5208/5229891101_b00b1e1427_b.jpg'
    };

  }

}
