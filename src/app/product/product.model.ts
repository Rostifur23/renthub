export class Product {
    public pid: number;
    public name: string;
    public type: string;
    public daily_charge: number;
    public availible: boolean;
    public prod_pic: string;

    constructor(pid: number, name: string, type: string, daily_charge: number, availible: boolean, prod_pic: string) {
        this.pid = pid;
        this.name = name;
        this.type = type;
        this.daily_charge = daily_charge;
        this.availible = availible;
        this.prod_pic = prod_pic;
    }
}