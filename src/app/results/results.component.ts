import { Component, OnInit } from '@angular/core';
import { Product } from '../product/product.model';



@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {


  results: Product[] = [
    new Product(1, 'product name', 'Technology', 39, true, 'https://c1.staticflickr.com/6/5208/5229891101_b00b1e1427_b.jpg'),
    new Product(1, 'product name', 'Technology', 39, true, 'https://c1.staticflickr.com/6/5208/5229891101_b00b1e1427_b.jpg'),
    new Product(1, 'product name', 'Technology', 39, true, 'https://c1.staticflickr.com/6/5208/5229891101_b00b1e1427_b.jpg'),
    new Product(1, 'product name', 'Technology', 39, true, 'https://c1.staticflickr.com/6/5208/5229891101_b00b1e1427_b.jpg'),
    new Product(1, 'product name', 'Technology', 39, true, 'https://c1.staticflickr.com/6/5208/5229891101_b00b1e1427_b.jpg'),
    new Product(1, 'product name', 'Technology', 39, true, 'https://c1.staticflickr.com/6/5208/5229891101_b00b1e1427_b.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }

}
