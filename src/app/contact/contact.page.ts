import { Component } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: 'contact.page.html',
  styleUrls: ['contact.page.scss']
})
export class ContactPage {

  username: string = 'UserName123';
  email: string = 'useremail123@gmail.com';

  username_form: boolean = true;
  email_form: boolean = true;

  new_username: string = this.username;
  new_email: string = this.email;
  

  changeUBoolean (status: boolean) {

    if(status){
      this.username_form = false;
    } else {
      this.username_form = true;
    }
    
  }

  changeEBoolean (status: boolean) {

    if(status){
      this.email_form = false;
    } else {
      this.email_form = true;
    }
    
  }

  onUsernameChange(event: Event) {
    this.new_username = (<HTMLInputElement>event.target).value;
    
  }

  onEmailChange(event: Event) {
    this.new_email = (<HTMLInputElement>event.target).value;
    
  }

  changeEmail() {

    this.email = this.new_email;
    this.email_form = true;

  }

  changeUsername() {

    this.username = this.new_username;
    this.username_form = true;

  }
  

}
