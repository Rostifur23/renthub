import { Component, OnInit } from '@angular/core';
import {Category} from './category.model';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: Category[] = [
    new Category('Technology', 'https://images.pexels.com/photos/48606/pexels-photo-48606.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
    new Category('Car Rentals', 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Cars_for_sale_%286046441241%29.jpg/1600px-Cars_for_sale_%286046441241%29.jpg'),
    new Category('Home Improvement', 'https://c.pxhere.com/photos/43/e3/crafts_diy_drill_measuring_tape_screws_tools_workbench-991634.jpg!d')
  ];

  constructor() { }

  ngOnInit() {
  }

}
