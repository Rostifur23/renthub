export class Category {
    public name: string;
    public imagePath: string;

    constructor(name: string, img: string) {
        this.name = name;
        this.imagePath = img;
    }
}