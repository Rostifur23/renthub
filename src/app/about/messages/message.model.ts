export class Message {
    public username: string;
    public msg: string;

    constructor(username: string, msg: string) {
        this.username = username;
        this.msg = msg;
    }
}