import { Component, OnInit } from '@angular/core';
import { Message } from './message.model';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  messages: Message[] = [
    new Message('Username', 'Hello there how are you?'),
    new Message('Username', 'Are you still looking at this item?'),
    new Message('Username', 'The product should be availible to pick up on tuesday.'),
    new Message('Username', 'Do you need any help finding the location?'),
    new Message('Username', 'Hi, I am interested in your advert.')
  ];

  constructor() { }

  ngOnInit() {
  }

}
