import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { HomePage } from '../home/home.page';
import { AboutPage } from '../about/about.page';
import { ContactPage } from '../contact/contact.page';
import { ResultsComponent } from '../results/results.component';
import { ProductComponent } from '../product/product.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: '',
        redirectTo: '/tabs/(home:home)',
        pathMatch: 'full',
      },
      {
        path: 'home',
        outlet: 'home',
        component: HomePage
      },
      {
        path: 'about',
        outlet: 'about',
        component: AboutPage
      },
      {
        path: 'contact',
        outlet: 'contact',
        component: ContactPage
      }
    ]
  },
  {
    path: 'results',
    component: ResultsComponent
  },
  {
    path: 'product/:pid',
    component: ProductComponent
  },
  {
    path: '',
    redirectTo: '/tabs/(home:home)',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [ResultsComponent, ProductComponent],
  imports: [RouterModule.forChild(routes), CommonModule, IonicModule],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
